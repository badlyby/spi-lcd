# HPMicro SPI LCD LVGL DEMO

## 概述

这是一个LVGL性能测试DEMO，运行在HPM6300EVK开发板上，屏幕是LBS147TC  1.47寸IPS屏幕模块，分辨率320*172。 
开发环境使用hpm_sdk v1.4.0，需要把hpm_sdk\middleware\lvgl\lvgl文件夹复制到工程文件夹才能运行，sdk内联lvgl会关联LCD接口，为了使用SPI接口我重写了porting。 
默认build_type是flash_sdram_xip开启片外SDRAM可以达到24fps。 
如果使用片内SRAM需要修改flash_xip.icf文件把AXI_SRAM改大，片内SRAM可以跑到30多帧。 

## flash_xip.icf文件修改下面两行
```
define region AXI_SRAM  = [from 0x01080000 size 384k];
define region NONCACHEABLE_RAM = [from 0x010E0000 size 128k];
```

## 硬件连接
| 开发板 |  | 屏幕模块 |
| :---: | :---: | :---: |
| PC18 | -> | CS |
| PC20 | -> | SCL |
| PC21 | -> | SDA |
| PC26 | -> | RES |
| PC27 | -> | DC |

[视频地址](https://b23.tv/BIAWlTN)